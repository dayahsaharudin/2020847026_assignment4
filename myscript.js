function submitAnswers(){
    //User input
    var question1 = document.forms["cssquiz"]["question1"].value;
    var question2 = document.forms["cssquiz"]["question2"].value;
    var question3 = document.forms["cssquiz"]["question3"].value;
    var question4 = document.forms["cssquiz"]["question4"].value;
    var question5 = document.forms["cssquiz"]["question5"].value;
    var question6 = document.forms["cssquiz"]["question6"].value;
    var question7 = document.forms["cssquiz"]["question7"].value;
    var question8 = document.forms["cssquiz"]["question8"].value;
    var question9 = document.forms["cssquiz"]["question9"].value;
    var question10 = document.forms["cssquiz"]["question10"].value;
    var name = document.forms["cssquiz"]["name"].value;

    var total = 10; 
    var score = 0;

    //Validation
    for(let i = 1; i<= total; i++){
        if(eval("question" +i) == null || eval("question" +i) == ""){
            alert("Oops!! Question " + i + " is required");
            return false;
        }
    }

    if (name == "" || name == null){
        alert("Oops!! Your name is required");
        return false;
    }

    //Set Answers
    var answers = ["d", "b", "c", "a", "b", "d", "a", "b", "c", "no"];

    //Check Answers
    for(i = 1; i <= total; i++){
        if(eval("question" +i) == answers[i-1]){
            score++;
        }
    }

    //Display results
    if(score >= 0 && score < 5 ){
        alert("Keep trying, " + name + "! You answered " + score + " out of 10 correctly");
    }
    else if(score > 4 && score < 10){
        alert("Way to go, " + name + "! You got " + score + " out of 10 correct");
    }
    else if(score == 10){
        alert("Congratulations " + name + "! You got 10 out of 10");
    }
}
